<?php

function randomPassword()
{
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = null;

    for ($i = 0; $i < 16; $i++) {
        $n = rand(0, strlen($alphabet) - 1);
        $pass[$i] = $alphabet[$n];
    }
    return implode('',$pass);
}

?>

<!DOCTYPE html>
<html>
<head>Password generator</head>
<body><h3>Password generator</h3>
<?php echo randomPassword(); ?>
</body>
</html>
